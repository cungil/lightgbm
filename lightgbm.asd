(in-package #:asdf)

(defsystem #:lightgbm
    :name "lightgbm"
    :author "Carlos Ungil <ungil@mac.com>"
    :license "Apache License, Version 2.0"
    :description "Common Lisp interface to https://github.com/Microsoft/LightGBM"
    :in-order-to ((test-op (test-op "lightgbm/test")))
    :depends-on (:trivial-garbage :cffi :cl-autowrap :alexandria)
    :serial t
    :components ((:module #:ffi :pathname "ffi" :components ((:static-file "c_api.h")))
		 (:file "package")
		 (:file "wrapper")
		 (:file "lightgbm")))

(defsystem #:lightgbm/test
    :name "lightgbm test suite"
    :author "Carlos Ungil <ungil@mac.com>"
    :license "Apache License, Version 2.0"
    :depends-on (:lightgbm :fiveam)
    :components ((:file "test"))
    :perform (asdf:test-op (o s) (declare (ignore o s)) (uiop:symbol-call :lightgbm.test '#:run)))
