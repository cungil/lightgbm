(in-package :lightgbm.test)

(defvar *testfiles-path* (merge-pathnames "examples/" (asdf:system-source-directory :lightgbm)))

(defun almost-equal (a b &optional (epsilon 1e-5))
  (if (and (listp a) (listp b))
      (let* ((matching (mapcar #'almost-equal a b))
	     (errors (count nil matching))
	     (len (length matching)))
	(when (and (plusp errors) (> len 10) (< errors len))
	  (terpri)
	  (warn "~A out of ~A elements ~A different" errors len (if (> errors 1) "are" "is"))
	  (loop for matches in matching
		for i from 0
		unless matches
		  do (print i)
		     (print (elt a i))
		     (print (elt b i)))
	  (terpri))
	(every #'identity matching))
      (cond ((and (numberp a) (numberp b))
	     (or (and (zerop a) (zerop b))
		 (and (not (zerop a)) (not (zerop b))
		      (< (/ (abs (- a b)) (+ a b)) epsilon))))
	    ((and (stringp a) (stringp b))
	     (string= (remove #\Newline (remove #\Return (remove #\Linefeed a)))
		      (remove #\Newline (remove #\Return (remove #\Linefeed b)))))
	    (t (equal a b)))))

(defun run ()
  (5am:run! 'lightgbm-suite))

(5am:def-suite lightgbm-suite)

(5am:in-suite lightgbm-suite)

(5am:test read-data
  (5am:is (equal '(7000 28)
		 (dims (read-data-file (merge-pathnames "binary.train" *testfiles-path*))))))

(5am:test empty-dataset
  (5am:is (equal '(42 28)
		 (dims (empty-dataset (read-data-file (merge-pathnames "binary.train" *testfiles-path*)) 42)))))

(5am:test num-classes
  (5am:is (= 1 (num-classes (booster (read-data-file (merge-pathnames "binary.train" *testfiles-path*)))))))

(5am:test feature-names
  (5am:is (equal (feature-names (read-data-file (merge-pathnames "binary.train" *testfiles-path*)))
		 '("Column_0" "Column_1" "Column_2" "Column_3" "Column_4" "Column_5" "Column_6" "Column_7" "Column_8" "Column_9"
		   "Column_10" "Column_11" "Column_12" "Column_13" "Column_14" "Column_15" "Column_16" "Column_17" "Column_18"
		   "Column_19" "Column_20" "Column_21" "Column_22" "Column_23" "Column_24" "Column_25" "Column_26" "Column_27"))))

(5am:test feature-names-set
  (5am:is (equal (let ((dataset (read-data-file (merge-pathnames "binary.train" *testfiles-path*))))
		   (set-feature-names dataset (loop for i from 1 repeat (nfeatures dataset) collect (format nil "test~A" i)))
		   (feature-names dataset))
		 '("test1" "test2" "test3" "test4" "test5" "test6" "test7" "test8" "test9" "test10" "test11" "test12" "test13" "test14" "test15"
		   "test16" "test17" "test18" "test19" "test20" "test21" "test22" "test23" "test24" "test25" "test26" "test27" "test28"))))

(5am:test feature-names-booster
  (5am:is (equal (feature-names (booster (read-data-file (merge-pathnames "binary.train" *testfiles-path*))))
		 '("Column_0" "Column_1" "Column_2" "Column_3" "Column_4" "Column_5" "Column_6" "Column_7" "Column_8" "Column_9"
		   "Column_10" "Column_11" "Column_12" "Column_13" "Column_14" "Column_15" "Column_16" "Column_17" "Column_18"
		   "Column_19" "Column_20" "Column_21" "Column_22" "Column_23" "Column_24" "Column_25" "Column_26" "Column_27"))))

(5am:test update-param-check
  (5am:signals simple-error (lightgbm::dataset-update-param-checking "is_sparse=true" "is_sparse=false")))

(5am:test booster-to-string
  (5am:is (almost-equal
	   "tree
version=v4
num_class=1
num_tree_per_iteration=1
label_index=0
max_feature_idx=27
objective=regression
feature_names=Column_0 Column_1 Column_2 Column_3 Column_4 Column_5 Column_6 Column_7 Column_8 Column_9 Column_10 Column_11 Column_12 Column_13 Column_14 Column_15 Column_16 Column_17 Column_18 Column_19 Column_20 Column_21 Column_22 Column_23 Column_24 Column_25 Column_26 Column_27
feature_infos=[0.27500000000000002:6.6950000000000003] [-2.4169999999999998:2.4300000000000002] [-1.7429999999999999:1.7429999999999999] [0.019:5.7000000000000002] [-1.7429999999999999:1.7429999999999999] [0.159:4.1900000000000004] [-2.9409999999999998:2.9699999999999998] [-1.7410000000000001:1.7410000000000001] [0:2.173] [0.19:5.1929999999999996] [-2.9039999999999999:2.9089999999999998] [-1.742:1.7429999999999999] [0:2.2149999999999999] [0.26400000000000001:6.5229999999999997] [-2.7279999999999998:2.7269999999999999] [-1.742:1.742] [0:2.548] [0.36499999999999999:6.0679999999999996] [-2.4950000000000001:2.496] [-1.74:1.7429999999999999] [0:3.1019999999999999] [0.17199999999999999:13.098000000000001] [0.41899999999999998:7.3920000000000003] [0.46100000000000002:3.6819999999999999] [0.38400000000000001:6.5830000000000002] [0.092999999999999999:7.8600000000000003] [0.38900000000000001:4.5430000000000001] [0.48899999999999999:4.3159999999999998]
tree_sizes=

end of trees

feature_importances:

parameters:
[boosting: gbdt]
[objective: regression]
[metric: l2]
[tree_learner: serial]
[device_type: cpu]
[data: ]
[valid: ]
[num_iterations: 100]
[learning_rate: 0.1]
[num_leaves: 31]
[num_threads: 0]
[deterministic: 0]
[force_col_wise: 0]
[force_row_wise: 0]
[histogram_pool_size: -1]
[max_depth: -1]
[min_data_in_leaf: 20]
[min_sum_hessian_in_leaf: 0.001]
[bagging_fraction: 1]
[pos_bagging_fraction: 1]
[neg_bagging_fraction: 1]
[bagging_freq: 0]
[bagging_seed: 3]
[feature_fraction: 1]
[feature_fraction_bynode: 1]
[feature_fraction_seed: 2]
[extra_trees: 0]
[extra_seed: 6]
[early_stopping_round: 0]
[first_metric_only: 0]
[max_delta_step: 0]
[lambda_l1: 0]
[lambda_l2: 0]
[linear_lambda: 0]
[min_gain_to_split: 0]
[drop_rate: 0.1]
[max_drop: 50]
[skip_drop: 0.5]
[xgboost_dart_mode: 0]
[uniform_drop: 0]
[drop_seed: 4]
[top_rate: 0.2]
[other_rate: 0.1]
[min_data_per_group: 100]
[max_cat_threshold: 32]
[cat_l2: 10]
[cat_smooth: 10]
[max_cat_to_onehot: 4]
[top_k: 20]
[monotone_constraints: ]
[monotone_constraints_method: basic]
[monotone_penalty: 0]
[feature_contri: ]
[forcedsplits_filename: ]
[refit_decay_rate: 0.9]
[cegb_tradeoff: 1]
[cegb_penalty_split: 0]
[cegb_penalty_feature_lazy: ]
[cegb_penalty_feature_coupled: ]
[path_smooth: 0]
[interaction_constraints: ]
[verbosity: 1]
[saved_feature_importance_type: 0]
[linear_tree: 0]
[max_bin: 255]
[max_bin_by_feature: ]
[min_data_in_bin: 3]
[bin_construct_sample_cnt: 200000]
[data_random_seed: 1]
[is_enable_sparse: 1]
[enable_bundle: 1]
[use_missing: 1]
[zero_as_missing: 0]
[feature_pre_filter: 1]
[pre_partition: 0]
[two_round: 0]
[header: 0]
[label_column: ]
[weight_column: ]
[group_column: ]
[ignore_column: ]
[categorical_feature: ]
[forcedbins_filename: ]
[precise_float_parser: 0]
[parser_config_file: ]
[objective_seed: 5]
[num_class: 1]
[is_unbalance: 0]
[scale_pos_weight: 1]
[sigmoid: 1]
[boost_from_average: 1]
[reg_sqrt: 0]
[alpha: 0.9]
[fair_c: 1]
[poisson_max_delta_step: 0.7]
[tweedie_variance_power: 1.5]
[lambdarank_truncation_level: 30]
[lambdarank_norm: 1]
[label_gain: ]
[lambdarank_position_bias_regularization: 0]
[eval_at: ]
[multi_error_top_k: 1]
[auc_mu_weights: ]
[num_machines: 1]
[local_listen_port: 12400]
[time_out: 120]
[machine_list_filename: ]
[machines: ]
[gpu_platform_id: -1]
[gpu_device_id: -1]
[gpu_use_dp: 0]
[num_gpu: 1]

end of parameters
"
	   (save (booster (read-data-file (merge-pathnames "binary.train" *testfiles-path*))) :string))))

(5am:test dump-model
  (5am:is (almost-equal "{\"name\":\"tree\",
\"version\":\"v4\",
\"num_class\":1,
\"num_tree_per_iteration\":1,
\"label_index\":0,
\"max_feature_idx\":27,
\"objective\":\"regression\",
\"average_output\":false,
\"feature_names\":[\"Column_0\",\"Column_1\",\"Column_2\",\"Column_3\",\"Column_4\",\"Column_5\",\"Column_6\",\"Column_7\",\"Column_8\",\"Column_9\",\"Column_10\",\"Column_11\",\"Column_12\",\"Column_13\",\"Column_14\",\"Column_15\",\"Column_16\",\"Column_17\",\"Column_18\",\"Column_19\",\"Column_20\",\"Column_21\",\"Column_22\",\"Column_23\",\"Column_24\",\"Column_25\",\"Column_26\",\"Column_27\"],
\"monotone_constraints\":[],
\"feature_infos\":{\"Column_0\":{\"min_value\":0.27500000000000002,\"max_value\":6.6950000000000003,\"values\":[]},\"Column_1\":{\"min_value\":-2.4169999999999998,\"max_value\":2.4300000000000002,\"values\":[]},\"Column_2\":{\"min_value\":-1.7429999999999999,\"max_value\":1.7429999999999999,\"values\":[]},\"Column_3\":{\"min_value\":0.019,\"max_value\":5.7000000000000002,\"values\":[]},\"Column_4\":{\"min_value\":-1.7429999999999999,\"max_value\":1.7429999999999999,\"values\":[]},\"Column_5\":{\"min_value\":0.159,\"max_value\":4.1900000000000004,\"values\":[]},\"Column_6\":{\"min_value\":-2.9409999999999998,\"max_value\":2.9699999999999998,\"values\":[]},\"Column_7\":{\"min_value\":-1.7410000000000001,\"max_value\":1.7410000000000001,\"values\":[]},\"Column_8\":{\"min_value\":0,\"max_value\":2.173,\"values\":[]},\"Column_9\":{\"min_value\":0.19,\"max_value\":5.1929999999999996,\"values\":[]},\"Column_10\":{\"min_value\":-2.9039999999999999,\"max_value\":2.9089999999999998,\"values\":[]},\"Column_11\":{\"min_value\":-1.742,\"max_value\":1.7429999999999999,\"values\":[]},\"Column_12\":{\"min_value\":0,\"max_value\":2.2149999999999999,\"values\":[]},\"Column_13\":{\"min_value\":0.26400000000000001,\"max_value\":6.5229999999999997,\"values\":[]},\"Column_14\":{\"min_value\":-2.7279999999999998,\"max_value\":2.7269999999999999,\"values\":[]},\"Column_15\":{\"min_value\":-1.742,\"max_value\":1.742,\"values\":[]},\"Column_16\":{\"min_value\":0,\"max_value\":2.548,\"values\":[]},\"Column_17\":{\"min_value\":0.36499999999999999,\"max_value\":6.0679999999999996,\"values\":[]},\"Column_18\":{\"min_value\":-2.4950000000000001,\"max_value\":2.496,\"values\":[]},\"Column_19\":{\"min_value\":-1.74,\"max_value\":1.7429999999999999,\"values\":[]},\"Column_20\":{\"min_value\":0,\"max_value\":3.1019999999999999,\"values\":[]},\"Column_21\":{\"min_value\":0.17199999999999999,\"max_value\":13.098000000000001,\"values\":[]},\"Column_22\":{\"min_value\":0.41899999999999998,\"max_value\":7.3920000000000003,\"values\":[]},\"Column_23\":{\"min_value\":0.46100000000000002,\"max_value\":3.6819999999999999,\"values\":[]},\"Column_24\":{\"min_value\":0.38400000000000001,\"max_value\":6.5830000000000002,\"values\":[]},\"Column_25\":{\"min_value\":0.093000000000000013,\"max_value\":7.8600000000000003,\"values\":[]},\"Column_26\":{\"min_value\":0.38900000000000001,\"max_value\":4.5430000000000001,\"values\":[]},\"Column_27\":{\"min_value\":0.48899999999999999,\"max_value\":4.3159999999999998,\"values\":[]}},
\"tree_info\":[],

\"feature_importances\":{}
}
"
			(dump-model (booster (read-data-file (merge-pathnames "binary.train" *testfiles-path*)))))))

(5am:test early-stopping
  (5am:is (= 7
	     (let* ((training (read-data-file (merge-pathnames "regression.train" *testfiles-path*)))
		    (validation (read-data-file (merge-pathnames "regression.test" *testfiles-path*) :reference training))
		    (params '(:task "train"
			      :boosting-type "gbdt"
			      :metric ("l2" "auc")
			      :num-leaves 31
			      :learning-rate 0.05
			      :feature-fraction 0.9
			      :bagging-fraction 0.8
			      :bagging-freq 5
			      :num-threads 1
			      :verbose -1))
		    (gbm (booster training params)))
	       (multiple-value-bind (metrics best) (train gbm 20 validation 2)
		 (declare (ignore metrics))
		 best)))))

(5am:test prediction
  (5am:is (almost-equal 0.47171512
	     (let* ((training (read-data-file (merge-pathnames "regression.train" *testfiles-path*)))
		    (validation (read-data-file (merge-pathnames "regression.test" *testfiles-path*) :reference training))
		    (params '(:task "train"
			      :boosting-type "gbdt"
			      :metric ("l2" "auc")
			      :num-leaves 31
			      :learning-rate 0.05
			      :feature-fraction 0.9
			      :bagging-fraction 0.8
			      :bagging-freq 5
			      :num-threads 1
			      :verbose -1))
		    (gbm (booster training params)))
	       (multiple-value-bind (metrics best) (train gbm 20 validation 2)
		 (declare (ignore metrics))
		 (multiple-value-bind (test-data test-labels)
		     (read-data-file-as-matrix (merge-pathnames "regression.test" *testfiles-path*))
		   (sqrt (/ (loop for (pred) in (predict-matrix gbm test-data :iterations best :type :normal)
			       for label in test-labels
			       sum (expt (- pred label) 2))
			    (length test-labels)))))))))

(5am:test data-file-by-row
  (5am:is (almost-equal 0.47171512
	     (let* ((training (read-data-file (merge-pathnames "regression.train" *testfiles-path*)))
		    (validation (read-data-file-row-by-row (merge-pathnames "regression.test" *testfiles-path*) :reference training))
		    (params '(:task "train"
			      :boosting-type "gbdt"
			      :metric ("l2" "auc")
			      :num-leaves 31
			      :learning-rate 0.05
			      :feature-fraction 0.9
			      :bagging-fraction 0.8
			      :bagging-freq 5
			      :num-threads 1
			      :verbose -1))
		    (gbm (booster training params)))
	       (multiple-value-bind (metrics best) (train gbm 20 validation 2)
		 (declare (ignore metrics))
		 (multiple-value-bind (test-data test-labels)
		     (read-data-file-as-matrix (merge-pathnames "regression.test" *testfiles-path*))
		   (sqrt (/ (loop for (pred) in (predict-matrix gbm test-data :iterations best :type :normal)
			       for label in test-labels
			       sum (expt (- pred label) 2))
			    (length test-labels)))))))))

(5am:test multiclass-predict
  (5am:is-true
   (let* ((training (read-data-file (merge-pathnames "multiclass.train" *testfiles-path*)))
	  (validation (read-data-file (merge-pathnames "multiclass.test" *testfiles-path*) :reference training))
	  (params '(:task "train"
		    :boosting-type "gbdt"
		    :objective "multiclass"
		    :metric "multi_logloss"
		    :num-class 5
		    :num-leaves 31
		    :learning-rate 0.05
		    :feature-fraction 0.9
		    :bagging-fraction 0.8
		    :bagging-freq 5
		    :num-threads 1
		    :verbose 0))
	  (gbm (booster training params)))
     (multiple-value-bind (metrics best) (train gbm 10 validation)
       (declare (ignore metrics best))
       (let ((tmpfile (merge-pathnames (make-pathname :name "out" :type "txt") (uiop:temporary-directory))))
	 (predict-file gbm (merge-pathnames "multiclass.test" *testfiles-path*) tmpfile)
	 (almost-equal (get-predictions gbm 1)
		       (let ((*read-default-float-format* 'double-float))
			 (with-open-file (in tmpfile)
			   (loop for line = (read-line in nil nil)
				 while line
				 collect (read-from-string (concatenate 'string "(" line ")")))))))))))

(5am:test multiclass-predict-matrix-2iter-normal
  (5am:is-true
   (let* ((training (read-data-file (merge-pathnames "multiclass.train" *testfiles-path*)))
	  (validation (read-data-file (merge-pathnames "multiclass.test" *testfiles-path*) :reference training))
	  (params '(:task "train"
		    :boosting-type "gbdt"
		    :objective "multiclass"
		    :metric "multi_logloss"
		    :num-class 5
		    :num-leaves 31
		    :learning-rate 0.05
		    :feature-fraction 0.9
		    :bagging-fraction 0.8
		    :bagging-freq 5
		    :num-threads 1
		    :verbose 0))
	  (gbm (booster training params)))
     (multiple-value-bind (metrics best) (train gbm 10 validation)
       (declare (ignore metrics best))
       (let ((tmpfile (merge-pathnames (make-pathname :name "out2n" :type "txt") (uiop:temporary-directory))))
	 (predict-file gbm (merge-pathnames "multiclass.test" *testfiles-path*) tmpfile :type :normal :iterations 2)
	 (multiple-value-bind (test-data test-labels)
	     (read-data-file-as-matrix (merge-pathnames "multiclass.test" *testfiles-path*))
	   (declare (ignore test-labels))
	   (almost-equal (predict-matrix gbm test-data :type :normal :iterations 2)
			 (let ((*read-default-float-format* 'double-float))
			   (with-open-file (in tmpfile)
			     (loop for line = (read-line in nil nil)
				   while line
				   collect (read-from-string (concatenate 'string "(" line ")"))))))))))))

(5am:test multiclass-predict-matrix-2iter-raw-score
  (5am:is-true
   (let* ((training (read-data-file (merge-pathnames "multiclass.train" *testfiles-path*)))
	  (validation (read-data-file (merge-pathnames "multiclass.test" *testfiles-path*) :reference training))
	  (params '(:task "train"
		    :boosting-type "gbdt"
		    :objective "multiclass"
		    :metric "multi_logloss"
		    :num-class 5
		    :num-leaves 31
		    :learning-rate 0.05
		    :feature-fraction 0.9
		    :bagging-fraction 0.8
		    :bagging-freq 5
		    :num-threads 1
		    :verbose 0))
	  (gbm (booster training params)))
     (multiple-value-bind (metrics best) (train gbm 10 validation)
       (declare (ignore metrics best))
       (let ((tmpfile (merge-pathnames (make-pathname :name "out2r" :type "txt") (uiop:temporary-directory))))       
	 (predict-file gbm (merge-pathnames "multiclass.test" *testfiles-path*) tmpfile :type :raw-score :iterations 2)
	 (multiple-value-bind (test-data test-labels)
	   (read-data-file-as-matrix (merge-pathnames "multiclass.test" *testfiles-path*))
	   (declare (ignore test-labels))
	   (almost-equal (predict-matrix gbm test-data :type :raw-score :iterations 2)
			 (let ((*read-default-float-format* 'double-float))
			   (with-open-file (in tmpfile)
			     (loop for line = (read-line in nil nil)
				   while line
				   collect (read-from-string (concatenate 'string "(" line ")"))))))))))))

(5am:test multiclass-predict-matrix-2iter-leaf-index
  (5am:is-true
   (let* ((training (read-data-file (merge-pathnames "multiclass.train" *testfiles-path*)))
	  (validation (read-data-file (merge-pathnames "multiclass.test" *testfiles-path*) :reference training))
	  (params '(:task "train"
		    :boosting-type "gbdt"
		    :objective "multiclass"
		    :metric "multi_logloss"
		    :num-class 5
		    :num-leaves 31
		    :learning-rate 0.05
		    :feature-fraction 0.9
		    :bagging-fraction 0.8
		    :bagging-freq 5
		    :num-threads 1
		    :verbose 0))
	  (gbm (booster training params)))
     (multiple-value-bind (metrics best) (train gbm 10 validation)
       (declare (ignore metrics best))
       (let ((tmpfile (merge-pathnames (make-pathname :name "out2l" :type "txt") (uiop:temporary-directory))))
	 (predict-file gbm (merge-pathnames "multiclass.test" *testfiles-path*) tmpfile :type :leaf-index :iterations 2)
	 (multiple-value-bind (test-data test-labels)
	     (read-data-file-as-matrix (merge-pathnames "multiclass.test" *testfiles-path*))
	   (declare (ignore test-labels))
	   (almost-equal (predict-matrix gbm test-data :type :leaf-index :iterations 2)
			 (let ((*read-default-float-format* 'double-float))
			   (with-open-file (in tmpfile)
			     (loop for line = (read-line in nil nil)
				   while line
				   collect (read-from-string (concatenate 'string "(" line ")"))))))))))))

(5am:test multiclass-predict-matrix-whole
  (5am:is-true
   (let* ((training (read-data-file (merge-pathnames "multiclass.train" *testfiles-path*)))
	  (validation (read-data-file (merge-pathnames "multiclass.test" *testfiles-path*) :reference training))
	  (params '(:task "train"
		    :boosting-type "gbdt"
		    :objective "multiclass"
		    :metric "multi_logloss"
		    :num-class 5
		    :num-leaves 31
		    :learning-rate 0.05
		    :feature-fraction 0.9
		    :bagging-fraction 0.8
		    :bagging-freq 5
		    :num-threads 1
		    :verbose 0))
	  (gbm (booster training params)))
     (multiple-value-bind (metrics best) (train gbm 10 validation)
       (declare (ignore best metrics))
       (let ((tmpfile (merge-pathnames (make-pathname :name "outM" :type "txt") (uiop:temporary-directory))))
	 (predict-file gbm (merge-pathnames "multiclass.test" *testfiles-path*) tmpfile :type :normal)
	 (multiple-value-bind (test-data test-labels)
	     (read-data-file-as-matrix (merge-pathnames "multiclass.test" *testfiles-path*))
	   (declare (ignore test-labels))
	   (warn "the alternative prediction methods may give similar but not completely identical results for some reason")
	   (almost-equal (predict-matrix gbm test-data :type :normal)
		       (let ((*read-default-float-format* 'double-float))
			 (with-open-file (in tmpfile)
			   (loop for line = (read-line in nil nil)
				 while line
				 collect (read-from-string (concatenate 'string "(" line ")"))))))))))))
