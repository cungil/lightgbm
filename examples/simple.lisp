(in-package :lightgbm)

;; to produce the charts R is required
;; (ql:quickload :rcl)

(let* ((lightgbm-path (merge-pathnames "LightGBM/" #-(and windows sbcl) (user-homedir-pathname)
				       #+(and windows sbcl) (cl:concatenate 'cl:string (sb-posix:getenv "USERPROFILE") "/")))
       (training (read-data-file (merge-pathnames "examples/regression/regression.train" lightgbm-path)))
       (validation (read-data-file (merge-pathnames "examples/regression/regression.test" lightgbm-path) :reference training))
       (params '(:task "train"
		 :boosting-type "gbdt"
		 :objective "regression"
		 :metric ("l2" "auc")
		 :num-leaves 31
		 :learning-rate 0.05
		 :feature-fraction 0.9
		 :bagging-fraction 0.8
		 :bagging-freq 5
		 :num-threads 1
		 :verbose 0))
       (gbm (booster training params)))
  (multiple-value-bind (metrics best) (train gbm 20 validation 5)
    (format t "~&Best iteration: ~A~&" best)
    (multiple-value-bind (test-data test-labels)
	(read-data-file-as-matrix (merge-pathnames "examples/regression/regression.test" lightgbm-path))
      (format t "~&RMSE of prediction: ~,7F"
	      (sqrt (/ (loop for (pred) in (predict-matrix gbm test-data :iterations best :type :normal) 
			  for label in test-labels
			  sum (expt (- pred label) 2))
		       (length test-labels))))
      ;;(print (get-predictions gbm))
      (format t "~&Feature importance split (full):~{~&~A~25T~A~}"
	      (mapcan #'list (feature-names gbm) (importance gbm :split)))
      (format t "~&Feature importance gain (full):~{~&~A~25T~A~}"
	      (mapcan #'list (feature-names gbm) (importance gbm :gain)))
      ;; (format t "~&Feature importance split (best):~{~&~A~25T~A~}"
      ;; 	      (mapcan #'list (feature-names gbm) (importance gbm :split best)))
      ;; (format t "~&Feature importance gain (best):~{~&~A~25T~A~}"
      ;;         (mapcan #'list (feature-names gbm) (importance gbm :gain best)))
      #+RCL(r:with-par (2 2)
	     (let ((dataset-names (remove-duplicates (mapcar #'first metrics) :test #'string=))
		   (metric-names (remove-duplicates (mapcar #'second metrics) :test #'string=)))
	       (loop for metric in metric-names
		  do (r:r "plot" (loop for s in metrics
				    when (and (string= (second s) metric)
					      (string= (first s) (first dataset-names)))
				    append (subseq s 2))
			  :ylim (r:r "range" (loop for s in metrics
						when (string= (second s) metric)
						append (subseq s 2)))
			  :main metric :xlab "" :ylab "" :bty "n" :type "l" :col 1)
		    (loop for dataset in (rest dataset-names)
		       for color from 2
		       do (r:r "lines" (loop for s in metrics
					  when (and (string= (second s) metric)
						    (string= (first s) dataset))
					  append (subseq s 2))
			       :xlab "" :ylab "" :bty "n" :col color))
		    (r:r "abline" :v best :col "gray")))
	     (rcl:r "barplot" (importance gbm :split best) :main "split" :horiz t)
	     (rcl:r "barplot" (importance gbm :gain best) :main "gain" :horiz t)))))

;; adapted from LightGBM/examples/python-guide/simple_example.py
;; python output, setting 'num_threads'=1 and adding importance calculation

;; Load data...
;; Start training...
;; [1]	valid_0's auc: 0.755797	valid_0's l2: 0.242963
;; Training until validation scores don't improve for 5 rounds.
;; [2]	valid_0's auc: 0.755071	valid_0's l2: 0.239442
;; [3]	valid_0's auc: 0.777275	valid_0's l2: 0.235933
;; [4]	valid_0's auc: 0.778678	valid_0's l2: 0.231946
;; [5]	valid_0's auc: 0.78183	valid_0's l2: 0.228124
;; [6]	valid_0's auc: 0.781387	valid_0's l2: 0.225515
;; [7]	valid_0's auc: 0.789441	valid_0's l2: 0.222806
;; [8]	valid_0's auc: 0.789095	valid_0's l2: 0.22004
;; [9]	valid_0's auc: 0.78999	valid_0's l2: 0.217652
;; [10]	valid_0's auc: 0.792046	valid_0's l2: 0.214958
;; [11]	valid_0's auc: 0.795867	valid_0's l2: 0.212286
;; [12]	valid_0's auc: 0.797472	valid_0's l2: 0.210131
;; [13]	valid_0's auc: 0.799689	valid_0's l2: 0.208102
;; [14]	valid_0's auc: 0.79856	valid_0's l2: 0.206802
;; [15]	valid_0's auc: 0.798826	valid_0's l2: 0.205296
;; [16]	valid_0's auc: 0.801374	valid_0's l2: 0.203473
;; [17]	valid_0's auc: 0.804405	valid_0's l2: 0.201494
;; [18]	valid_0's auc: 0.80759	valid_0's l2: 0.199964
;; [19]	valid_0's auc: 0.808461	valid_0's l2: 0.198698
;; [20]	valid_0's auc: 0.809533	valid_0's l2: 0.197279
;; Save model...
;; Start predicting...
;; The rmse of prediction is: 0.444161535406
;; Dump model to JSON...
;; Feature names: ['Column_0', 'Column_1', 'Column_2', 'Column_3', 'Column_4', 'Column_5', 'Column_6', 'Column_7', 'Column_8', 'Column_9', 'Column_10', 'Column_11', 'Column_12', 'Column_13', 'Column_14', 'Column_15', 'Column_16', 'Column_17', 'Column_18', 'Column_19', 'Column_20', 'Column_21', 'Column_22', 'Column_23', 'Column_24', 'Column_25', 'Column_26', 'Column_27']
;; Calculate feature importances...
;; Feature importances (split): [20, 10, 5, 28, 3, 59, 11, 4, 3, 25, 12, 10, 1, 16, 6, 4, 2, 15, 3, 10, 1, 30, 61, 8, 61, 79, 51, 62]
;; Feature importances (gain): [67.790457263818354, 24.964877896639173, 13.644771806081065, 99.481865599722667, 8.4054799085559804, 277.64430708908492, 27.817346224940025, 10.999587755841656, 12.215508922155321, 83.273375752901757, 32.50032979133001, 26.655997416475245, 2.3410281977309495, 49.237916060308898, 17.317893214322982, 12.051248072522132, 5.0917625987182475, 44.626069439527875, 6.9710670569614628, 28.640129598615289, 3.1076250049852909, 90.668483614967826, 265.89085882562972, 21.051460846074093, 271.81322676331382, 1089.4996466615946, 449.00743906458359, 458.49220307283031]
