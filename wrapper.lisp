(in-package :lightgbm.ffi)

;; to regenerate the autowrap definitions, delete or rename the .spec files
;; requires LLVM/Clang and c2ffi https://github.com/rpav/c2ffi

;; ffi/c_api.h is the one in include/LightGBM in the LightGBM sources
;; replacing the line:
;;   #include <LightGBM/export.h>
;; with the following:
;;   #define LIGHTGBM_C_EXPORT 
;;   typedef unsigned long long size_t;

(autowrap:c-include '(lightgbm ffi "c_api.h") :spec-path '(lightgbm ffi))

(cl:let ((library #-windows (cl:or (cl:probe-file "/opt/homebrew/Cellar/lightgbm/4.1.0/lib/lib_lightgbm.dylib")
				   (cl:probe-file "/usr/local/lib/lib_lightgbm.so")
				   (cl:probe-file (cl:merge-pathnames "LightGBM/lib_lightgbm.so" (cl:user-homedir-pathname))))
		  #+windows (cl:or (cl:probe-file (cl:merge-pathnames "lib_lightgbm.dll" (cl:user-homedir-pathname)))
				   (cl:probe-file (cl:merge-pathnames "LightGBM/windows/x64/DLL/lib_lightgbm.dll" (cl:user-homedir-pathname))))))
  (cl:if library
    (cl:and (cffi:load-foreign-library library)
	    (cl:format cl:t "~%~%   Foreign library ~A loaded~%~%~%" library))
    (cl:error "lightgbm library not found, edit wrapper.lisp file")))
