(in-package :lightgbm)

;; LGBM_BoosterRefit
;; https://github.com/microsoft/LightGBM/commit/2db6377a227b78a03044237f0c599cd65627b9f9

;; LGBM_DumpParamAliases
;; LGBM_ByteBufferGetAt
;; LGBM_ByteBufferFree

;; LGBM_DatasetCreateFromSampledColumn
;; LGBM_DatasetCreateFromMats
;; LGBM_DatasetCreateFromCSC
;; LGBM_DatasetCreateFromCSRFunc
;; LGBM_DatasetInitStreaming
;; LGBM_DatasetCreateFromSerializedReference
;; LGBM_DatasetPushRowsWithMetadata
;; LGBM_DatasetPushRowsByCSRWithMetadata
;; LGBM_DatasetSetWaitForManualFinish
;; LGBM_DatasetMarkFinished
;; LGBM_DatasetSerializeReferenceToBinary
;; LGBM_DatasetGetFeatureNumBin

;; LGBM_BoosterGetLinear
;; LGBM_BoosterGetLoadedParam
;; LGBM_BoosterPredictForCSC
;; LGBM_BoosterPredictForCSRSingleRow
;; LGBM_BoosterPredictForMatSingleRow
;; LGBM_BoosterPredictForMats
;; LGBM_BoosterValidateFeatureNames


;; LGBM_FastConfigFree
;; LGBM_BoosterPredictForCSRSingleRowFastInit
;; LGBM_BoosterPredictForCSRSingleRowFast
;; LGBM_BoosterPredictForMatSingleRowFastInit
;; LGBM_BoosterPredictForMatSingleRowFast

(defmacro with-output-value ((name type) &body body)
  `(cffi:with-foreign-object (,name ,type)
     ,@body
     (cffi:mem-ref ,name ,type)))

(defmacro with-output-values (bindings &body body)
  `(cffi:with-foreign-objects
       ,(loop for (name type) in bindings collect (list name type))
     ,@body
     (list ,@(loop for (name type) in bindings
		collect `(cffi:mem-ref ,name ,type)))))

(defmacro with-output-array ((name type count) &body body)
  `(cffi:with-foreign-objects ((,name :pointer) (,count :int))
     ,@body
     (loop for i below (cffi:mem-ref ,count :int)
	collect (cffi:mem-aref (cffi:mem-ref ,name :pointer) ,type i))))

(defun check-errors (x)
  (unless (zerop x)
    (error "~A" (lgbm.ffi:lgbm-get-last-error))))

(defgeneric save (object file &optional start-iteration n-iterations importance-type))

(defclass lgbm-pointer ()
  ((pointer :initarg :pointer :accessor pointer)))

(defclass lgbm-booster (lgbm-pointer)
  ())

(defclass lgbm-dataset (lgbm-pointer)
  ())

(defvar *boosters*
  (tg:make-weak-hash-table :weakness :value))

(defvar *datasets*
  (tg:make-weak-hash-table :weakness :value))

(defmethod save ((dataset lgbm-dataset) file &optional start-iteration n-iterations importance-type)
  (declare (ignore start-iteration n-iterations))
  (check-errors (lgbm.ffi:lgbm-dataset-save-binary (pointer dataset) (namestring file))))

(defmethod save-text ((dataset lgbm-dataset) file)
  (check-errors (lgbm.ffi:lgbm-dataset-dump-text (pointer dataset) (namestring file))))

(defmethod nrow ((dataset lgbm-dataset))
  (with-output-value (nrow :int)
    (check-errors (lgbm.ffi:lgbm-dataset-get-num-data (pointer dataset) nrow))))

(defmethod nfeatures ((dataset lgbm-dataset))
  (with-output-value (nfeature :int)
    (check-errors (lgbm.ffi:lgbm-dataset-get-num-feature (pointer dataset) nfeature))))

(defmethod dims ((dataset lgbm-dataset))
  (list (nrow dataset) (nfeatures dataset)))

(defmethod field-int32 ((dataset lgbm-dataset) field)
  (with-output-array (res :int32 len)
    (assert (= lgbm.ffi:+C-API-DTYPE-INT32+
	       (with-output-value (type :int)
		 (check-errors (lgbm.ffi:lgbm-dataset-get-field (pointer dataset) field len res type)))))))

(defmethod field-float ((dataset lgbm-dataset) field)
  (with-output-array (res :float len)
    (assert (= lgbm.ffi:+C-API-DTYPE-FLOAT32+
	       (with-output-value (type :int)
		 (check-errors (lgbm.ffi:lgbm-dataset-get-field (pointer dataset) field len res type)))))))

(defmethod field-float64 ((dataset lgbm-dataset) field)
  (with-output-array (res :double len)
    (assert (= lgbm.ffi:+C-API-DTYPE-FLOAT64+
	       (with-output-value (type :int)
		 (check-errors (lgbm.ffi:lgbm-dataset-get-field (pointer dataset) field len res type)))))))

(defmethod field ((dataset lgbm-dataset) field)
  (ecase field
    (:label (field-float dataset "label"))
    (:weight (field-float dataset "weight"))
    (:init-score (field-float64 dataset "init_score"))
    (:group (field-int32 dataset "group"))
    (:group-id (field-int32 dataset "group_id"))))

(defmethod set-field-int32 ((dataset lgbm-dataset) field values)
  (let ((type lgbm.ffi:+C-API-DTYPE-INT32+))
    (cffi:with-foreign-object (data :int32 (length values))
      (loop for i fixnum from 0 for val in values
	 do (setf (cffi:mem-aref data :int32 i) (coerce val 'integer)))
      (check-errors (lgbm.ffi:lgbm-dataset-set-field (pointer dataset) field data (length values) type)))))

(defmethod set-field-float ((dataset lgbm-dataset) field values)
  (let ((type lgbm.ffi:+C-API-DTYPE-FLOAT32+))
    (cffi:with-foreign-object (data :float (length values))
      (loop for i fixnum from 0 for val in values
	 do (setf (cffi:mem-aref data :float i) (coerce val 'single-float)))
      (check-errors (lgbm.ffi:lgbm-dataset-set-field (pointer dataset) field data (length values) type)))))

(defmethod set-field-float64 ((dataset lgbm-dataset) field values)
  (let ((type lgbm.ffi:+C-API-DTYPE-FLOAT64+))
    (cffi:with-foreign-object (data :double (length values))
      (loop for i fixnum from 0 for val in values
	 do (setf (cffi:mem-aref data :double i) (coerce val 'double-float)))
      (check-errors (lgbm.ffi:lgbm-dataset-set-field (pointer dataset) field data (length values) type)))))

(defmethod set-field ((dataset lgbm-dataset) field values)
  (assert (= (length values) (nrow dataset)))
  (ecase field
    (:label (set-field-float dataset "label" values))
    (:weight (set-field-float dataset "weight" values))
    (:init-score (set-field-float64 dataset "init_score" values))
    (:group (set-field-int32 dataset "group" values))
    (:group-id (set-field-int32 dataset "group_id" values)))
  dataset)

(defmethod print-object ((dataset lgbm-dataset) stream)
  (print-unreadable-object (dataset stream :type t)
    (format stream "~Ax~A~{ ~A~}" (nrow dataset) (nfeatures dataset)
	    (loop for field in '(:label :weight :init-score :group)
	       ;; :group-id)  -- trying to retrieve group_id raises an error!!
	       when (and (field dataset field) (or (not (eq field :label))
						   (not (every #'zerop (field dataset :label)))))
	       collect field))))

(defun dataset-update-param-checking (old new)
  (check-errors (lgbm.ffi:lgbm-dataset-update-param-checking old new)))

(defmethod slice ((dataset lgbm-dataset) rows &optional (parameters ""))
  ;; rows are of the form 1:nrow ??
  (lgbm-dataset
   (with-output-value (handle :pointer)
     (cffi:with-foreign-object (idxset :int32 (length rows))
       (loop for row in (sort (copy-seq rows) #'<) for i from 0
	  do (setf (cffi:mem-aref idxset :int32 i) row))
       (check-errors (lgbm.ffi:lgbm-dataset-get-subset (pointer dataset) idxset (length rows) parameters handle))))))

(defmethod feature-names ((dataset lgbm-dataset))
  (let ((nfeatures (nfeatures dataset))
	(buffer-length 100))
    (cffi:with-foreign-object (names :pointer nfeatures)
      (loop for i below nfeatures
	 do (setf (cffi:mem-aref names :pointer i) (cffi:foreign-alloc :char :count buffer-length)))
      (let ((required (with-output-value (buffer-required :unsigned-long-long)
			(assert (= nfeatures
				   (with-output-value (nout :int)
				      (check-errors (lgbm.ffi:lgbm-dataset-get-feature-names (pointer dataset) nfeatures nout
											     buffer-length buffer-required names))))))))
	(if (> required buffer-length)
	    (warn "buffer length ~A insufficient to retrieve dataset feature names, ~A required" buffer-length required)))
      (loop for i below nfeatures
	 collect (cffi:foreign-string-to-lisp (cffi:mem-aref names :pointer i))
	 do (cffi:foreign-string-free (cffi:mem-aref names :pointer i))))))

(defmethod set-feature-names ((dataset lgbm-dataset) feature-names)
  (let ((nfeatures (nfeatures dataset)))
    (assert (= (length feature-names) nfeatures))
    (cffi:with-foreign-object (names :pointer nfeatures)
      (loop for i below nfeatures for name in feature-names
	 do (setf (cffi:mem-aref names :pointer i) (cffi:foreign-string-alloc name)))
      (check-errors (lgbm.ffi:lgbm-dataset-set-feature-names (pointer dataset) names nfeatures))
      (loop for i below nfeatures
	 do (cffi:foreign-string-free (cffi:mem-aref names :pointer i)))))
  dataset)

(defun lgbm-dataset (ptr)
  (assert (cffi:pointerp ptr))
  (let ((dataset (make-instance 'lgbm-dataset :pointer ptr)))
    (tg:finalize dataset (lambda () (lgbm.ffi:lgbm-dataset-free ptr)))
    (setf (gethash (cffi:pointer-address ptr) *datasets*) dataset)))

(defun read-data-file (file &key (parameters "") reference ignore-init-file)
  (assert (or (null reference) (typep reference 'lgbm-dataset)))
  (let ((dataset (lgbm-dataset
		  (with-output-value (handle :pointer)
		    (check-errors (lgbm.ffi:lgbm-dataset-create-from-file (namestring file) parameters (when reference (pointer reference)) handle))))))
    (when ignore-init-file
      (warn "FIXME: to completely ignore the .init file need to set to false the parameter :boost-from-average (see https://github.com/Microsoft/LightGBM/issues/1101)")
      (set-field dataset :init-score (loop repeat (nrow dataset) collect 0)))
    dataset))

(defmethod empty-dataset ((reference lgbm-dataset) nrows)
  (lgbm-dataset
   (with-output-value (handle :pointer)
     (check-errors (lgbm.ffi:lgbm-dataset-create-by-reference (pointer reference) nrows handle)))))

(defmethod add-features ((target lgbm-dataset) (source lgbm-dataset))
  (check-errors (lgbm.ffi:lgbm-dataset-add-features-from (pointer target) (pointer source)))
  target)

(defmethod push-rows ((dataset lgbm-dataset) rows start-row &optional (data-type :float))
  ;; there are no checks for size/type consistency with the dataset?
  (assert (and (listp rows) (= 1 (length (remove-duplicates (mapcar #'length rows))))))
  (let ((ncol (length (first rows)))
	(nrow (length rows))
	(lgbm-type (ecase data-type
		     (:float lgbm.ffi:+C-API-DTYPE-FLOAT32+)
		     (:double lgbm.ffi:+C-API-DTYPE-FLOAT64+))))
    (ecase data-type
      (:float (cffi:with-foreign-object (data :float (* nrow ncol))
		(loop for i fixnum below (* nrow ncol)
		   for value in (apply #'append rows)
		   do (setf (cffi:mem-aref data :float i) (coerce value 'single-float)))
		(check-errors (lgbm.ffi:lgbm-dataset-push-rows (pointer dataset) data lgbm-type nrow ncol start-row))))
      (:double (cffi:with-foreign-object (data :double (* nrow ncol))
		(loop for i fixnum below (* nrow ncol)
		   for value in (apply #'append rows)
		   do (setf (cffi:mem-aref data :double i) (coerce value 'double-float)))
		(check-errors (lgbm.ffi:lgbm-dataset-push-rows (pointer dataset) data lgbm-type nrow ncol start-row))))))
  dataset)

(defmethod push-rows-by-csr ((dataset lgbm-dataset) data column-indices row-starts ncol start-row &optional (data-type :float))
  (let ((lgbm-type (ecase data-type
		     (:float lgbm.ffi:+C-API-DTYPE-FLOAT32+)
		     (:double lgbm.ffi:+C-API-DTYPE-FLOAT64+)))
	(indptr-type lgbm.ffi:+C-API-DTYPE-INT64+))
    (ecase data-type
      (:float (cffi:with-foreign-objects ((dat :float (length data))
					  (cid :unsigned-int (length column-indices))
					  (rst :unsigned-long (length row-starts)))
		(loop for i fixnum from 0 for d in data
		   do (setf (cffi:mem-aref dat :float i) (coerce d 'single-float)))
		(loop for i fixnum from 0 for ci in column-indices
		   do (setf (cffi:mem-aref cid :unsigned-int i) (coerce ci 'integer)))
		(loop for i fixnum from 0 for rs in row-starts
		   do (setf (cffi:mem-aref rst :unsigned-long i) (coerce rs 'integer)))
		(check-errors (lgbm.ffi:lgbm-dataset-push-rows-by-csr
			       (pointer dataset) rst indptr-type cid dat lgbm-type
			       (length row-starts) (length data) ncol start-row))))
      (:double (cffi:with-foreign-objects ((dat :double (length data))
					   (cid :unsigned-int (length column-indices))
					   (rst :unsigned-long (length row-starts)))
		 (loop for i fixnum from 0 for d in data
		    do (setf (cffi:mem-aref dat :double i) (coerce d 'double-float)))
		 (loop for i fixnum from 0 for ci in column-indices
		    do (setf (cffi:mem-aref cid :unsigned-int i) (coerce ci 'integer)))
		 (loop for i fixnum from 0 for rs in row-starts
		    do (setf (cffi:mem-aref rst :unsigned-long i) (coerce rs 'integer)))
		 (check-errors (lgbm.ffi:lgbm-dataset-push-rows-by-csr
				(pointer dataset) rst indptr-type cid dat lgbm-type
				(length row-starts) (length data) ncol start-row))))))
  dataset)

(defun dataset-from-matrix (mat &optional (parameters "") (data-type :float) reference)
  (declare (optimize speed))
  (assert (or (null reference) (typep reference 'lgbm-dataset)))
  (assert (typep mat 'array))
  (let ((nrow (array-dimension mat 0))
	(ncol (array-dimension mat 1))
	(lgbm-type (ecase data-type
		     (:float lgbm.ffi:+C-API-DTYPE-FLOAT32+)
		     (:double lgbm.ffi:+C-API-DTYPE-FLOAT64+)))
	(is-row-major 1))
    (lgbm-dataset
     (with-output-value (handle :pointer)
       (ecase data-type
	 (:float (cffi:with-foreign-object (data :float (* nrow ncol))
		   (loop for i fixnum below (* nrow ncol)
		      do (setf (cffi:mem-aref data :float i) (coerce (row-major-aref mat i) 'single-float)))
		   (check-errors (lgbm.ffi:lgbm-dataset-create-from-mat
				  data lgbm-type nrow ncol is-row-major
				  parameters (when reference (pointer reference)) handle))))
	 (:double (cffi:with-foreign-object (data :double (* nrow ncol))
		    (loop for i fixnum below (* nrow ncol)
		       do (setf (cffi:mem-aref data :double i) (coerce (row-major-aref mat i) 'double-float)))
		    (check-errors (lgbm.ffi:lgbm-dataset-create-from-mat
				   data lgbm-type nrow ncol is-row-major
				   parameters (when reference (pointer reference)) handle)))))))))

;; from CSR / Compressed Sparse Row
;; http://www.scipy-lectures.org/advanced/scipy_sparse/csr_matrix.html#examples

;; (dataset-from-csr '(1 2 3 4 5 6) '(0 2 2 0 1 2) '(0 2 3 6) 3)
;; the first row is (1 0 2)
;; the second row is (0 0 3)
;; the third row is (4 5 6)

(defun dataset-from-csr (data column-indices row-starts ncol &optional (parameters "") (data-type :float) reference)
  ;; ncol is ignored, if there are empty columns on the right they won't be included in the output
  (assert (or (null reference) (typep reference 'lgbm-dataset)))
  (let ((lgbm-type (ecase data-type
		     (:float lgbm.ffi:+C-API-DTYPE-FLOAT32+)
		     (:double lgbm.ffi:+C-API-DTYPE-FLOAT64+)))
	(indptr-type lgbm.ffi:+C-API-DTYPE-INT64+))
    (lgbm-dataset
     (with-output-value (handle :pointer)
       (ecase data-type
	 (:float (cffi:with-foreign-objects ((dat :float (length data))
					     (cid :unsigned-int (length column-indices))
					     (rst :unsigned-long (length row-starts)))
		   (loop for i fixnum from 0 for d in data
		      do (setf (cffi:mem-aref dat :float i) (coerce d 'single-float)))
		   (loop for i fixnum from 0 for ci in column-indices
		      do (setf (cffi:mem-aref cid :unsigned-int i) (coerce ci 'integer)))
		   (loop for i fixnum from 0 for rs in row-starts
		      do (setf (cffi:mem-aref rst :unsigned-long i) (coerce rs 'integer)))
		   (check-errors (lgbm.ffi:lgbm-dataset-create-from-csr
				  rst indptr-type cid dat lgbm-type (length row-starts) (length data) ncol
				  parameters (when reference (pointer reference)) handle))))
	 (:double (cffi:with-foreign-objects ((dat :double (length data))
					      (cid :unsigned-int (length column-indices))
					      (rst :unsigned-long (length row-starts)))
		    (loop for i fixnum from 0 for d in data
		       do (setf (cffi:mem-aref dat :double i) (coerce d 'double-float)))
		    (loop for i fixnum from 0 for ci in column-indices
		       do (setf (cffi:mem-aref cid :unsigned-int i) (coerce ci 'integer)))
		    (loop for i fixnum from 0 for rs in row-starts
		       do (setf (cffi:mem-aref rst :unsigned-long i) (coerce rs 'integer)))
		    (check-errors (lgbm.ffi:lgbm-dataset-create-from-csr
				   rst indptr-type cid dat lgbm-type (length row-starts) (length data) ncol
				   parameters (when reference (pointer reference)) handle)))))))))

(defun lgbm-booster (ptr)
  (assert (cffi:pointerp ptr))
  (let ((booster (make-instance 'lgbm-booster :pointer ptr)))
    (tg:finalize booster (lambda () (lgbm.ffi:lgbm-booster-free ptr)))
    (setf (gethash (cffi:pointer-address ptr) *boosters*) booster)))

(defun parameters-string (parameters)
  (format nil "~{~A~^ ~}"
	  (loop for (key value) on parameters by #'cddr
	     collect (let ((key (if (symbolp key)
				    (substitute #\_ #\- (string-downcase (symbol-name key)))
				    key))
			   (value (if (and (listp value) (not (null value)))
				      (format nil "~{~A~^,~}" value)
				    (typecase value
				      (boolean (if value "true" "false"))
				      (t value)))))
		       (format nil "~A=~A" key value)))))

(defmethod booster ((dataset lgbm-dataset) &optional (parameters ""))
  (when (listp parameters)
    (setf parameters (parameters-string parameters)))
  (lgbm-booster
   (with-output-value (handle :pointer)
     (check-errors (lgbm.ffi:lgbm-booster-create (pointer dataset) parameters handle)))))

(defmethod linear-p ((booster lgbm-booster))
  (= 1 (with-output-value (out :int)
	  (check-errors (lgbm.ffi:lgbm-dataset-get-num-data (pointer booster) out)))))

(defparameter *default-buffer-length* 4096)

(defmethod dump-model ((booster lgbm-booster) &optional (start-iteration 0) (n-iterations 0) ;; 0: save all
		                              (importance-type :split) (buffer-length *default-buffer-length*))
  (assert (member importance-type '(:split :gain)))
  (setf importance-type (position importance-type '(:split :gain)))
  (cffi:with-foreign-object (buffer :char buffer-length)
    (if (< buffer-length
	   (with-output-value (output-length :int)
	     (check-errors (lgbm.ffi:lgbm-booster-dump-model (pointer booster) start-iteration n-iterations importance-type buffer-length output-length buffer))))
	(dump-model booster start-iteration n-iterations (nth importance-type '(:split :gain)) (* 2 buffer-length))
	(cffi:foreign-string-to-lisp buffer))))

(defun save-model-to-string (booster &optional (start-iteration 0) (n-iterations 0) ;; 0: save all
				     (importance-type :split) (buffer-length *default-buffer-length*))
  (assert (member importance-type '(:split :gain)))
  (setf importance-type (position importance-type '(:split :gain)))
  (cffi:with-foreign-object (buffer :char buffer-length)
    (if (< buffer-length
	   (with-output-value (output-length :int)
	     (check-errors (lgbm.ffi:lgbm-booster-save-model-to-string (pointer booster) start-iteration n-iterations importance-type buffer-length output-length buffer))))
	(save-model-to-string booster start-iteration n-iterations (nth importance-type '(:split :gain)) (* 2 buffer-length))
	(cffi:foreign-string-to-lisp buffer))))

(defmethod save ((booster lgbm-booster) file &optional (start-iteration 0) (n-iterations 0) (importance-type :split)) ;; 0: save all
  (assert (member importance-type '(:split :gain)))
  (setf importance-type (position importance-type '(:split :gain)))  
  (if (or (null file) (eq :string file))
      (save-model-to-string booster start-iteration n-iterations)
      (check-errors (lgbm.ffi:lgbm-booster-save-model (pointer booster) start-iteration n-iterations importance-type (namestring file)))))

(defmethod booster ((file-or-string t) &optional parameters)
  (assert (null parameters))
  (destructuring-bind (booster iterations) 
      (with-output-values ((handle :pointer) (iterations :int))
	(if (or (pathnamep file-or-string) (probe-file file-or-string))
	    (check-errors (lgbm.ffi:lgbm-booster-create-from-modelfile (namestring file-or-string) iterations handle))
	    (check-errors (lgbm.ffi:lgbm-booster-load-model-from-string file-or-string iterations handle))))
    (values (lgbm-booster booster) iterations)))

(defmethod shuffle-models ((booster lgbm-booster) start end)
  (check-errors (lgbm.ffi:lgbm-booster-shuffle-models (pointer booster) start end))
  booster)

(defmethod merge-boosters ((booster lgbm-booster) (another-booster lgbm-booster))
  (check-errors (lgbm.ffi:lgbm-booster-merge (pointer booster) (pointer another-booster)))
  booster)

(defmethod add-validation-data ((booster lgbm-booster) (dataset lgbm-dataset))
  (check-errors (lgbm.ffi:lgbm-booster-add-valid-data (pointer booster) (pointer dataset)))
  booster)

(defmethod reset-training-data ((booster lgbm-booster) (dataset lgbm-dataset))
  (check-errors (lgbm.ffi:lgbm-booster-reset-training-data (pointer booster) (pointer dataset)))
  booster)

(defmethod reset-parameters ((booster lgbm-booster) &optional (parameters ""))
  (assert (stringp parameters))
  (check-errors (lgbm.ffi:lgbm-booster-reset-parameter (pointer booster) parameters))
  booster)

(defmethod num-classes ((booster lgbm-booster))
  (with-output-value (nclasses :int)
    (check-errors (lgbm.ffi:lgbm-booster-get-num-classes (pointer booster) nclasses))))

(defmethod num-trees-per-iteration ((booster lgbm-booster))
  (with-output-value (trees :int)
    (check-errors (lgbm.ffi:lgbm-booster-num-model-per-iteration (pointer booster) trees))))

(defmethod num-total-models ((booster lgbm-booster))
  (with-output-value (models :int)
    (check-errors (lgbm.ffi:lgbm-booster-number-of-total-model (pointer booster) models))))

(defmethod update-one-iteration ((booster lgbm-booster))
  (with-output-value (finished :int)
    (check-errors (lgbm.ffi:lgbm-booster-update-one-iter (pointer booster) finished))))

(defmethod update-one-iteration-custom ((booster lgbm-booster) grad hess)
  (assert (= (length grad) (length hess)))
  (cffi:with-foreign-objects ((%grad :float (length grad))
			      (%hess :float (length hess)))
    (loop for i from 0 for g in grad for h in hess
       do (setf (cffi:mem-aref %grad :float i) g
		(cffi:mem-aref %hess :float i) h))
    (with-output-value (finished :int)
      (check-errors (lgbm.ffi:lgbm-booster-update-one-iter-custom  (pointer booster) %grad %hess finished)))))

(defmethod rollback-one-iteration ((booster lgbm-booster))
  (check-errors (lgbm.ffi:lgbm-booster-rollback-one-iter (pointer booster)))
  booster)

(defmethod current-iteration ((booster lgbm-booster))
  (with-output-value (iteration :int)
    (check-errors (lgbm.ffi:lgbm-booster-get-current-iteration (pointer booster) iteration))))

(defmethod neval ((booster lgbm-booster))
  (with-output-value (count :int)
    (check-errors (lgbm.ffi:lgbm-booster-get-eval-counts (pointer booster) count))))

(defmethod eval-names ((booster lgbm-booster))
  (let ((neval (neval booster))
	(buffer-length 100))
    (cffi:with-foreign-object (names :pointer neval)
      (loop for i below neval
	 do (setf (cffi:mem-aref names :pointer i) (cffi:foreign-alloc :char :count buffer-length)))
      (let ((required (with-output-value (buffer-required :unsigned-long-long)
			(assert (= neval
				   (with-output-value (nout :int)
				      (check-errors (lgbm.ffi:lgbm-booster-get-eval-names (pointer booster) neval nout
											  buffer-length buffer-required names))))))))
	(if (> required buffer-length)
	    (warn "buffer length ~A insufficient to retrieve booster eval names, ~A required" buffer-length required)))
      (loop for i below neval
	 collect (cffi:foreign-string-to-lisp (cffi:mem-aref names :pointer i))
	 do (cffi:foreign-string-free (cffi:mem-aref names :pointer i))))))

(defmethod nfeatures ((booster lgbm-booster))
  (with-output-value (nfeature :int)
    (check-errors (lgbm.ffi:lgbm-booster-get-num-feature (pointer booster) nfeature))))

(defmethod feature-names ((booster lgbm-booster))
  (let ((nfeatures (nfeatures booster))
	(buffer-length 100))
    (cffi:with-foreign-object (names :pointer nfeatures)
      (loop for i below nfeatures
	 do (setf (cffi:mem-aref names :pointer i) (cffi:foreign-alloc :char :count buffer-length)))
      (let ((required (with-output-value (buffer-required :unsigned-long-long)
			(assert (= nfeatures
				   (with-output-value (nout :int)
				      (check-errors (lgbm.ffi:lgbm-booster-get-feature-names (pointer booster) nfeatures nout
											     buffer-length buffer-required names))))))))
	(if (> required buffer-length)
	    (warn "buffer length ~A insufficient to retrieve booster feature names, ~A required" buffer-length required)))
      (loop for i below nfeatures
	 collect (cffi:foreign-string-to-lisp (cffi:mem-aref names :pointer i))
	 do (cffi:foreign-string-free (cffi:mem-aref names :pointer i))))))

(defmethod get-eval ((booster lgbm-booster) &optional (idx 0)) ;; 1,2,3... for validation datasets
  (let* ((names (eval-names booster))
	 (n (length names)))
    (cffi:with-foreign-object (results :double n)
      (assert (= n (with-output-value (length :int)
		     (check-errors (lgbm.ffi:lgbm-booster-get-eval (pointer booster) idx length results)))))
      (loop for name in names for i from 0 collect (cons name (cffi:mem-aref results :double i))))))

(defmethod num-predictions ((booster lgbm-booster) &optional (idx 0)) ;; 1,2,3... for validation datasets
  (with-output-value (n :int64)
    (check-errors (lgbm.ffi:lgbm-booster-get-num-predict (pointer booster) idx n))))

(defmethod get-predictions ((booster lgbm-booster) &optional (idx 0))
  (let* ((npredictions (num-predictions booster idx)) 
	 (nclasses (num-classes booster)))
    (cffi:with-foreign-object (results :double npredictions)
      (assert (= npredictions
		 (with-output-value (n :int64)
		   (check-errors (lgbm.ffi:lgbm-booster-get-predict (pointer booster) idx n results)))))
      (apply #'mapcar #'list 
	     (loop repeat nclasses with i = 0
		collect (loop repeat (/ npredictions nclasses)
			   collect (cffi:mem-aref results :double i) do (incf i)))))))

(defmethod calc-num-predictions ((booster lgbm-booster) nrow &key (type :normal) (start-iteration 0) (iterations 0)) ;; 0: nolimit
  (assert (member type '(:normal :raw-score :leaf-index :contribution)))
  (let ((predict-type (ecase type
			(:normal lgbm.ffi:+C-API-PREDICT-NORMAL+) ;; 0
			(:raw-score lgbm.ffi:+C-API-PREDICT-RAW-SCORE+) ;; 1
			(:leaf-index lgbm.ffi:+C-API-PREDICT-LEAF-INDEX+) ;;2
			(:contribution lgbm.ffi:+C-API-PREDICT-CONTRIB+)))) ;;3
    (with-output-value (n :int64)
      (check-errors (lgbm.ffi:lgbm-booster-calc-num-predict (pointer booster) nrow predict-type start-iteration iterations n)))))

(defmethod predict-file ((booster lgbm-booster) input output &key (start-iteration 0) (iterations 0) ;; 0: nolimit
			 (type :normal) (parameters "") input-file-has-header)
  (assert (member type '(:normal :raw-score :leaf-index :contribution)))
  (let ((predict-type (ecase type
			(:normal lgbm.ffi:+C-API-PREDICT-NORMAL+) ;; 0
			(:raw-score lgbm.ffi:+C-API-PREDICT-RAW-SCORE+) ;; 1
			(:leaf-index lgbm.ffi:+C-API-PREDICT-LEAF-INDEX+) ;;2
    			(:contribution lgbm.ffi:+C-API-PREDICT-CONTRIB+)))) ;;3
    (check-errors (lgbm.ffi:lgbm-booster-predict-for-file (pointer booster) (namestring input)
							  (if input-file-has-header 1 0) predict-type
							  start-iteration iterations parameters (namestring output)))))

(defmethod predict-matrix ((booster lgbm-booster) mat &key (start-iteration 0) (iterations 0) ;; 0: nolimit
			   (type :normal) (parameters "") (data-type :float) (result-type 'single-float))
  (assert (typep mat 'array))
  (assert (member type '(:normal :raw-score :leaf-index :contribution)))
  (let* ((nrow (array-dimension mat 0))
	 (ncol (array-dimension mat 1))
	 (lgbm-type (ecase data-type
		      (:float lgbm.ffi:+C-API-DTYPE-FLOAT32+)
		      (:double lgbm.ffi:+C-API-DTYPE-FLOAT64+)))
	 (is-row-major 1)
	 (predict-type (ecase type
			 (:normal lgbm.ffi:+C-API-PREDICT-NORMAL+) ;; 0
			 (:raw-score lgbm.ffi:+C-API-PREDICT-RAW-SCORE+) ;; 1
			 (:leaf-index lgbm.ffi:+C-API-PREDICT-LEAF-INDEX+) ;; 2
			 (:contribution lgbm.ffi:+C-API-PREDICT-CONTRIB+))) ;; 3
    	 (output-length (calc-num-predictions booster nrow :type type
					      :start-iteration start-iteration :iterations iterations)))
    (cffi:with-foreign-object (results :double #-(and darwin ccl) output-length
				       ;; FIXME: why does example/simple crash if just output-length?
				       #+(and darwin ccl) (1+ output-length)) 
      (ecase data-type
	(:float (cffi:with-foreign-object (data :float (* nrow ncol))
		  (loop for i fixnum below (* nrow ncol)
		     do (setf (cffi:mem-aref data :float i) (coerce (row-major-aref mat i) 'single-float)))
		  (assert (= output-length
			     (with-output-value (length :int64)
			       (check-errors (lgbm.ffi:lgbm-booster-predict-for-mat
					      (pointer booster) data lgbm-type nrow ncol is-row-major
					      predict-type start-iteration iterations parameters length results)))))))
	(:double (cffi:with-foreign-object (data :double (* nrow ncol))
		  (loop for i fixnum below (* nrow ncol)
		     do (setf (cffi:mem-aref data :double i) (coerce (row-major-aref mat i) 'double-float)))
		  (assert (= output-length
			     (with-output-value (length :int64)
			       (check-errors (lgbm.ffi:lgbm-booster-predict-for-mat
					      (pointer booster) data lgbm-type nrow ncol is-row-major
					      predict-type start-iteration iterations parameters length results))))))))
      (loop repeat nrow with i = 0
	 collect (loop repeat (/ output-length nrow)
		    collect (coerce (cffi:mem-aref results :double i) result-type)
		    do (incf i))))))

(defmethod predict-csr ((booster lgbm-booster) data column-indices row-starts ncol
			&key (start-iteration 0) (iterations 0) (type :normal) (parameters "")
			(data-type :float) (result-type 'single-float))
  (assert (member type '(:normal :raw-score :leaf-index :contribution)))
  (let* ((nrow (1- (length row-starts)))
	 (lgbm-type (ecase data-type
		      (:float lgbm.ffi:+C-API-DTYPE-FLOAT32+)
		      (:double lgbm.ffi:+C-API-DTYPE-FLOAT64+)))
	 (indptr-type lgbm.ffi:+C-API-DTYPE-INT64+)
	 (predict-type (ecase type
			 (:normal lgbm.ffi:+C-API-PREDICT-NORMAL+) ;; 0
			 (:raw-score lgbm.ffi:+C-API-PREDICT-RAW-SCORE+) ;; 1
			 (:leaf-index lgbm.ffi:+C-API-PREDICT-LEAF-INDEX+) ;; 2
			 (:contribution lgbm.ffi:+C-API-PREDICT-CONTRIB+))) ;; 3
	 (output-length (calc-num-predictions booster nrow :type type
					      :start-iteration start-iteration :iterations iterations)))
    (cffi:with-foreign-object (results :double #-(and darwin ccl) output-length
				       #+(and darwin ccl) (1+ output-length))
      (ecase data-type
	(:float (cffi:with-foreign-objects ((dat :float (length data))
					    (cid :unsigned-int (length column-indices))
					    (rst :unsigned-long (length row-starts)))
		  (loop for i fixnum from 0 for d in data
		     do (setf (cffi:mem-aref dat :float i) (coerce d 'single-float)))
		  (loop for i fixnum from 0 for ci in column-indices
		     do (setf (cffi:mem-aref cid :unsigned-int i) (coerce ci 'integer)))
		  (loop for i fixnum from 0 for rs in row-starts
		     do (setf (cffi:mem-aref rst :unsigned-long i) (coerce rs 'integer)))
		  (assert (= output-length
			     (with-output-value (length :int64)
			       (check-errors (lgbm.ffi:lgbm-booster-predict-for-csr
					      (pointer booster) rst indptr-type cid dat lgbm-type (length row-starts)
					      (length data) ncol predict-type start-iteration iterations
					      parameters length results)))))))
	(:double (cffi:with-foreign-objects ((dat :double (length data))
					     (cid :unsigned-int (length column-indices))
					     (rst :unsigned-long (length row-starts)))
		   (loop for i fixnum from 0 for d in data
		      do (setf (cffi:mem-aref dat :double i) (coerce d 'double-float)))
		   (loop for i fixnum from 0 for ci in column-indices
		      do (setf (cffi:mem-aref cid :unsigned-int i) (coerce ci 'integer)))
		   (loop for i fixnum from 0 for rs in row-starts
		      do (setf (cffi:mem-aref rst :unsigned-long i) (coerce rs 'integer)))
		   (assert (= output-length
			      (with-output-value (length :int64)
				(check-errors (lgbm.ffi:lgbm-booster-predict-for-csr
					       (pointer booster) rst indptr-type cid dat lgbm-type (length row-starts)
					       (length data) ncol predict-type start-iteration iterations
					       parameters length results))))))))
      (loop repeat nrow with i = 0
	 collect (loop repeat (/ output-length nrow)
		    collect (coerce (cffi:mem-aref results :double i) result-type)
		    do (incf i))))))

(defmethod leaf-value ((booster lgbm-booster) tree leaf)
  (with-output-value (value :double)
    (check-errors (lgbm.ffi:lgbm-booster-get-leaf-value (pointer booster) tree leaf value))))

(defmethod set-leaf-value ((booster lgbm-booster) tree leaf value)
  (check-errors (lgbm.ffi:lgbm-booster-get-leaf-value (pointer booster) tree leaf value)))

(defmethod train ((booster lgbm-booster) num-rounds &optional validation-sets early-stopping-rounds)
  "Iterates until num-rounds or until any metric fails to improve in any of the validation sets for early-stopping-rounds.
Returns two values, a list of lists like (dataset metric x1 x2 ...) and the best iteration (or num-rounds if not stopped)."
  (unless (listp validation-sets)
    (setf validation-sets (list validation-sets)))
  (loop for valid in validation-sets
     do (add-validation-data booster valid))
  (loop repeat num-rounds
     with results = (make-hash-table :test #'equal)
     with best-value-so-far = (make-hash-table :test #'equal)
     with count-no-improvement = (make-hash-table :test #'equal)
     do (update-one-iteration booster)
       (format t "~&[~A]~5T" (1- (current-iteration booster)))
       (loop for i upto (length validation-sets)
	  for set = "train" then (format nil "valid-~A" i)
	  do (loop for (name . value) in (get-eval booster i)
		do (format t "  ~A ~A: ~,6F" set name value)
		  (push value (gethash (list set name) results))
		  (when (and early-stopping-rounds (not (string= set "train")))
		    (let ((best-so-far (gethash (list set name) best-value-so-far)))
		      (if (or (null best-so-far)
			      (if (member name '("auc" "map" "ndcg") :test #'string=)
				  (> value best-so-far)
				  (< value best-so-far)))
			  (setf (gethash (list set name) best-value-so-far) value
				(gethash (list set name) count-no-improvement) 0)
			  (incf (gethash (list set name) count-no-improvement)))))))
       (terpri)
       (force-output)
     until (and early-stopping-rounds (>= (loop for key being the hash-keys of count-no-improvement
					     maximize (gethash key count-no-improvement))
					  early-stopping-rounds))
     finally (return (values (loop for key being the hash-keys of results
				collect (append key (reverse (gethash key results))))
			     (when early-stopping-rounds
			       (if (< (current-iteration booster) num-rounds)
				   (- (current-iteration booster) 1 early-stopping-rounds)
				   num-rounds))))))

(defmethod importance ((booster lgbm-booster) type &optional (iteration 0))
  "Depening on the type parameter, returns the number of times a feature is used
in a model (:split) or the total gain for the splits using the feature (:gain).  
When iteration is set to zero (default) or negative the full model is used."
  (let ((type (ecase type (:split 0) (:gain 1)))
	(n (nfeatures booster)))
    (cffi:with-foreign-object (results :double n)
      (check-errors (lgbm.ffi:lgbm-booster-feature-importance (pointer booster) iteration type results))
      (loop for i below n collect (cffi:mem-aref results :double i)))))

;; (defmethod importance ((booster lgbm-booster) type &optional (iteration 0))
;;   (assert (member type '(:split :gain)))
;;   (let* ((model (let ((*read-default-float-format* 'double-float))
;; 		  (cl-json:decode-json-from-string (dump-model booster iteration))))
;; 	 (output (make-list (1+ (cdr (assoc :max--feature--idx model))) :initial-element 0)))
;;     (labels ((process (tree)
;; 	       (when (assoc :split--feature tree)
;; 		 (when (plusp (cdr (assoc :split--gain tree)))
;; 		   (incf (elt output (cdr (assoc :split--feature tree)))
;; 			 (if (eq type :split)
;; 			     1
;; 			     (cdr (assoc :split--gain tree)))))
;; 		 (process (cdr (assoc :left--child tree)))
;; 		 (process (cdr (assoc :right--child tree))))))
;;       (loop for tree in (cdr (assoc :tree--info model))
;; 	 do (process (cdr (assoc :tree--structure tree)))))
;;     output))

(defun read-data-file-row-by-row (file &key reference (includes-labels t))
  (assert reference)
  (let ((data (let ((*read-default-float-format* 'double-float))
	        (with-open-file (in file)
	  	   (loop for line = (read-line in nil nil)
			while line
			collect (read-from-string (concatenate 'string "(" line ")"))))))
	labels)
    (when includes-labels
      (setf labels (mapcar #'first data)
	    data (mapcar #'rest data)))
    (let ((output (empty-dataset reference (length data))))
      (loop for row in data
	 for i from 0
	 do (push-rows output (list row) i))
      (set-field output :label labels))))

(defun read-data-file-as-matrix (file &key (includes-labels t))
  (let ((data (let ((*read-default-float-format* 'double-float))
		(with-open-file (in file)
		   (loop for line = (read-line in nil nil)
			while line
			collect (read-from-string (concatenate 'string "(" line ")"))))))
	labels)
    (when includes-labels
      (setf labels (mapcar #'first data)
	    data (mapcar #'rest data)))
    (values (make-array (list (length data) (length (first data)))
			:initial-contents data)
	    labels)))

#|

;; from CSC / Compressed Sparse Column
;; http://www.scipy-lectures.org/advanced/scipy_sparse/csc_matrix.html#examples

(defun create-matrix-csc (data row-indices column-starts rows)
  (xgb-matrix
   (with-output-value (handle :pointer)
     (cffi:with-foreign-objects ((dat :float (length data))
				 (rid :unsigned-int (length row-indices))
				 (cst :unsigned-long (length column-starts)))
       (loop for i below (length data)
	  do (setf (cffi:mem-aref dat :float i) (coerce (elt data i) 'single-float)))
       (loop for i below (length row-indices)
	  do (setf (cffi:mem-aref rid :unsigned-int i) (coerce (elt row-indices i) 'integer)))
       (loop for i below (length column-starts)
	  do (setf (cffi:mem-aref cst :unsigned-long i) (coerce (elt column-starts i) 'integer)))
       (check-errors (xgb.ffi:xgd-matrix-create-from-csc-ex
		      cst rid dat (length column-starts) (length data) rows handle))))))

;; (create-matrix-csc '(1 4 5 2 3 6) '(0 2 2 0 1 2) '(0 2 3 6) 3)
;; the first column is (1 0 4), the second column in (0 0 5), the third column is (2 3 6)
;; the first row is (1 0 2)
;; the second row is (0 0 3)
;; the third row is (4 5 6)

|#

(defun init-network (machines local-port timeout)
  "machines should be a string of the form ip1:port1,ip2:port2
or a list of pairs ( IP address (string) . port (number) )"
  (unless (stringp machines)
    (setf machines (format nil "~{~A~^,~}" (loop for (ip . port) in machines
						 collect (format nil "~A:~A" ip port)))))
  (check-errors (lgbm.ffi:lgbm-network-init machines local-port timeout (count #\: machines))))

(defun free-network ()
  (check-errors (lgbm.ffi:lgbm-network-free)))
