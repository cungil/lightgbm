(defpackage #:lightgbm
  (:nicknames #:lgbm)
  (:use #:cl)
  (:export "READ-DATA-FILE" "DATASET-FROM-MATRIX" "EMPTY-DATASET" "PUSH-ROWS"
	   "DATASET-FROM-CSR" "PUSH-ROWS-BY-CSR" ;; "DATASET-FROM-CSC" 
	   "READ-DATA-FILE-ROW-BY-ROW" ;; "DATASET-FROM-SAMPLED-COLUMN"
	   "SLICE" "FIELD" "SET-FIELD" "NROW" "NFEATURES" "DIMS"
	   "BOOSTER" "SAVE" "DUMP-MODEL" "MERGE-BOOSTERS"
	   "RESET-PARAMETERS" "RESET-TRAINING-DATA" "ADD-VALIDATION-DATA"
	   "FEATURE-NAMES" "SET-FEATURE-NAMES" "NUM-CLASSES"
	   "NEVAL" "EVAL-NAMES" "GET-EVAL" "LEAF-VALUE" "SET-LEAF-VALUE"
	   "NUM-PREDICTIONS" "GET-PREDICTIONS" "CALC-NUM-PREDICTIONS"
	   "PREDICT-FILE" "PREDICT-MATRIX" "PREDICT-CSR" ;; "PREDICT-CSC"
	   "UPDATE-ONE-ITERATION" "UPDATE-ONE-ITERATION-CUSTOM"
	   "CURRENT-ITERATION" "ROLLBACK-ONE-ITERATION"
	   "READ-DATA-FILE-AS-MATRIX" "TRAIN" "IMPORTANCE"
	   ))

(defpackage #:lightgbm.ffi
  (:use)
  (:nicknames #:lgbm.ffi))

(defpackage #:lightgbm.test
  (:use #:cl #:lightgbm)
  (:export "RUN"))
